import { Component } from "react";
import BubbleAlert from "../BubbleAlert";
import DetalleCarro from "../DetalleCarro";

const styles = {
  button: {
    backgroundColor: '#359A2C',
    color: 'white',
    border: 'none',
    padding: '15px',
    cursor: 'pointer',
    borderRadius: '15px',
  },
  bubble: {
    position: 'relative',
    left: 18,
    top: 20,
  },
}

class ButtonCar extends Component {
  render(){
    const {carro, visible, setVisible} = this.props;
    const total = carro.reduce((acc, element) => acc + element.cantidad, 0);
    
    return (
      <div>
          <span style={styles.bubble}>
            {total !== 0 ? 
              <BubbleAlert value={total}/>
              : null
            }
          </span>
          <button style={styles.button} onClick={() => {setVisible()}} >
            Carro
          </button>
          {
            visible ?
            <DetalleCarro carro={carro} />
            : null
          }
      </div>
    );
  }
}

export default ButtonCar;