import { Component } from "react";
import ButtonCar from "../ButtonCar";
import Logo from "../Logo/Logo";

const styles = {
  navbar: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    height: '100px',
    justifyContent: 'space-between',
    paddingLeft: '50px',
    paddingRight: '50px',
    boxShadow: '0px 5px 5px rgb(0, 0, 0, 0.1)',
    position: 'relative', // sin este no muestra sombra
  }
}

class Navbar extends Component {
  render() {
    const { carro, visible, setVisible } = this.props;
    
    return(
      <div style={styles.navbar}>
        <Logo />
        <ButtonCar
          carro={carro}
          visible={visible}
          setVisible={setVisible}
        />
      </div>
    );
  }
}

export default Navbar;