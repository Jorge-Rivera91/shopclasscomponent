import { Component } from "react";
import Button from "../Button";

const styles = {
    producto: {
        marginTop: '16px',
        border: 'solid 1px #eee',
        boxShadow: '0 5px 5px rgb(0, 0, 0, 0.3)',
        padding: '10px 15px',
        borderRadius: '15px',
        width: '30%',
    },
    img: {
        width: '100%'
    }
}

class Producto extends Component {
    render () {
        const {agregarAlCarro, producto} = this.props;
        const {name, price, img} = producto;

        return(
            <div style={styles.producto}>
                <img style={styles.img} alt={`Img ${name}`} src={img} />
                <h3>{name}</h3>
                <p>{price}</p>
                <Button 
                    onClick={() => agregarAlCarro(producto)}
                    producto={producto}
                >
                    Agregar al Carro
                </Button>
            </div>
        );
    }
}

export default Producto;